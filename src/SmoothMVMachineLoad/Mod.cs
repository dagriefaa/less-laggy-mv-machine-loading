using System;
using Modding;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SmoothMVMachineLoad
{
	public class Mod : ModEntryPoint
	{

        int blocksPerFrame;

		public override void OnLoad()
		{
            Messages.SetBlocksPerFrame = ModNetworking.CreateMessageType(new DataType[] {
                DataType.Integer
            });
            ModNetworking.Callbacks[Messages.SetBlocksPerFrame] += msg => {
                OptionsMaster.blocksPerFrame = (int)msg.GetData(0);
            };

            ModConsole.RegisterCommand("SetBlocksPerFrame", OnBlocksPerFrame, "usage: SetBlocksPerFrame <int>");

            blocksPerFrame = (!Configuration.GetData().HasKey("BlocksPerFrame")) ? 5 : Configuration.GetData().ReadInt("BlocksPerFrame");
            OptionsMaster.blocksPerFrame = blocksPerFrame;

            Events.OnPlayerJoin += OnPlayerJoin;
            ReferenceMaster.OnDisconnect += OnDisconnect;
        }

        private void OnDisconnect() {
            OptionsMaster.blocksPerFrame = blocksPerFrame;
        }

        private void OnPlayerJoin(Modding.Common.Player player) {
            ModNetworking.SendTo(player, Messages.SetBlocksPerFrame.CreateMessage(new object[] { blocksPerFrame }));
        }

        void OnBlocksPerFrame(string[] args) {

            int.TryParse(args[0], out blocksPerFrame);
            if (StatMaster.isMP) {
                if (StatMaster.isClient) {
                    Debug.LogWarning("warning: server uses host setting for this option");
                }
                else {
                    ModNetworking.SendToAll(Messages.SetBlocksPerFrame.CreateMessage(new object[] { blocksPerFrame }));
                    OptionsMaster.blocksPerFrame = blocksPerFrame;
                }
            }
            else {
                Debug.LogWarning("warning: this option only applies in multiplayer");
                OptionsMaster.blocksPerFrame = blocksPerFrame;
            }

            Modding.Configuration.GetData().Write("BlocksPerFrame", blocksPerFrame);
        }
	}
}
