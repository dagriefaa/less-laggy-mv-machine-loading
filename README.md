# Less Laggy MV Machine Loading
https://steamcommunity.com/sharedfiles/filedetails/?id=2415988706

A problem with multiplayer in Besiege is that if somebody loads a complex machine, the server lags horrendously - because even though it spreads the loading out over time, it's still too fast.

This mod makes the loading speed configurable, and sets the default value to something reasonable.

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
All you really need to do is install the mod, and it will Just Work™.

However, it does add a console command `SetBlocksPerFrame` which allows you to specify how many blocks the game tries to load per frame, if you think it's too fast/slow.
